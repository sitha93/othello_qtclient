#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H


// local
#include "gamemodel.h"

// othello library
#include <engine.h>

// qt
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QSizeF>

// stl
#include <memory>


class GuiApplication : public QGuiApplication {
  Q_OBJECT
public:
  GuiApplication(int& argc, char** argv);
  ~GuiApplication() override = default;

private:
  othello::OthelloGameEngine m_game_engine;
  GameModel m_model;
  QQmlApplicationEngine m_app;

private slots:
  void initNewHumanGame();
  void endGameAndQuit();
  void endOfGameActions();
  void startNextTurn();
  void rectangleClicked(int rectangleId);
  void initMonkeyAIGame();

signals:
  void gameEnded();
  void displayFinalScores(int player_one_score, int player_two_score);
  void boardClicked(int);
  void initNewGameVsMonkeyAI();
  void enqueueNextTurn();


};   // END class GuiApplication

#endif   // GUIAPPLICATION_H
