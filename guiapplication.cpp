#include "guiapplication.h"

// othello game library
#include <orangemonkey_ai.h>


// qt
#include <QQmlContext>
#include <QQuickItem>
#include <QQuickWindow>
#include <QTimer>
#include <QDebug>
#include <QThread>

// stl
#include <chrono>

#include <iostream>
using namespace std::chrono_literals;


GuiApplication::GuiApplication(int& argc, char** argv)
  : QGuiApplication(argc, argv),
    m_game_engine{}, m_model{m_game_engine}, m_app{}
{

  m_app.rootContext()->setContextProperty("gamemodel", &m_model);

  m_app.load(QUrl("qrc:/qml/gui.qml"));

  auto* root_window = qobject_cast<QQuickWindow*>(m_app.rootObjects().first());
  if (root_window) {

    connect(root_window, SIGNAL(endGameAndQuit()), this,
            SLOT(endGameAndQuit()));

    connect(root_window, SIGNAL(initNewHumanGame()), this,
            SLOT(initNewHumanGame()));

    connect(this, &GuiApplication::gameEnded, this,
            &GuiApplication::endOfGameActions);

    connect(root_window, SIGNAL(boardClicked(int)), this,
             SLOT(rectangleClicked(int)));

    connect(root_window, SIGNAL(initNewGameVsMonkeyAI()), this,
            SLOT(initMonkeyAIGame()));

    connect(this, SIGNAL(displayFinalScores(int, int)), root_window,
            SIGNAL(displayFinalScores(int, int)));

  }

}

void GuiApplication::startNextTurn()
{
    if(m_game_engine.legalMovesCheck(m_game_engine.board(), m_game_engine.currentPlayerId())){
        if(m_game_engine.legalMovesCheck(m_game_engine.board(),
                                         othello::PlayerId((size_t(m_game_engine.currentPlayerId()) + 1) % 2))) {
            std::cout << "game is over" << std::endl;
            endOfGameActions();
        } else {
            std::cout << "no legal moves, skip turn" << std::endl;
            m_game_engine.skipTurn();
            startNextTurn();
        }
    } else if(m_game_engine.currentPlayerType() == othello::PlayerType::AI){
        m_game_engine.think(std::chrono::seconds(3));
        m_model.update();
        startNextTurn();
    }

}

void GuiApplication::rectangleClicked(int rectangleId)
{

    if(m_game_engine.performMoveForCurrentHuman(othello::BitPos(rectangleId))) {
        m_model.update();
        startNextTurn();
    } else
        std::cout << "Not a legal move" << std::endl;

}

void GuiApplication::initMonkeyAIGame()
{
    m_game_engine.initPlayerType<othello::monkey_ais::OrangeMonkeyAI, othello::PlayerId::One>();
    m_game_engine.initPlayerType<othello::monkey_ais::OrangeMonkeyAI, othello::PlayerId::Two>();

    m_game_engine.initNewGame();
    m_model.update();
    startNextTurn();
}

void GuiApplication::initNewHumanGame()
{
    m_game_engine.initPlayerType<othello::HumanPlayer, othello::PlayerId::One>();
    m_game_engine.initPlayerType<othello::HumanPlayer, othello::PlayerId::Two>();

    m_game_engine.initNewGame();
    m_model.update();
}

void GuiApplication::endGameAndQuit() { QGuiApplication::quit(); }


void GuiApplication::endOfGameActions()
{
    int playerOnePoints = m_game_engine.countPoints(othello::PlayerId::One);
    int playerTwoPoints = m_game_engine.countPoints(othello::PlayerId::Two);

    emit displayFinalScores(playerOnePoints, playerTwoPoints);
}
